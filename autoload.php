<?php
spl_autoload_register(function($className) {
  $className = strtolower($className);
  $filepath = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
	require_once($filepath);
});
 ?>
