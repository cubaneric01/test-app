<?
require '../IDb/ioperation.php';
require '../IDb/factory.php';

$productName = isset($_GET["productName"])?$_GET["productName"]:"";

const DBDRIVER = "mysql";

$productTransformer = DBFactory::getDbInstance(DBDRIVER, array('host' => "192.168.64.2","port"=>"3306", "user"=>"temp", "password"=>"Pa11word" ));
$product = $productTransformer->getProductByName($productName);

$rowsFound = false;
if ($product->getName()) {
  $rowsFound = true;
}


echo json_encode(array('productDesc' => $product->getDesc(), 'productName' => $product->getName(), 'upc'=> $product->getUpc(), 'inventory'=>$product->getInventory(), "productsFound"=>$rowsFound ));
?>
