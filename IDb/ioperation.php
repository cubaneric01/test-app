
<?
require '../model/product.php';

interface IProductOperation {
  function getProductByID($id);
  function getProductByName($name);
  function getProductByUPC($upc);
  function getProductsByInventory($inventoryAmount);
}

interface IOrderOperation {
  function getOrderByID($id);
  function getProductByNumber($orderNumber);
  function getPaidOrders();
}


class MySqlProduct{

  private $host, $port, $user, $password, $connection;
  function __construct($host, $port, $user, $password) {
    $this->host = $host;
    $this->port = $port;
    $this->user = $user;
    $this->password = $password;

    $dbConnection = new PDO("mysql:host=192.168.64.2;dbname=library", "temp", "Pa11word");
    $this->connection = $dbConnection;
  }

  function getProductByID($id){
    $result = $this->connection->prepare("select * from product where ID= :id");
    $result->execute(array('id' => $id));
    $row = $result->fetch(PDO::FETCH_ASSOC);
    return new \Model\Product($row["Name"],$row["UPC"],$row["Inventory"],$row["Desc"]);
  }

  function getProductByName($name){
    $result = $this->connection->prepare("select * from product where Name= :name");
    $result->execute(array('name' => $name));
    $row = $result->fetch(PDO::FETCH_ASSOC);
    return new \Model\Product($row["Name"],$row["UPC"],$row["Inventory"],$row["Desc"]);
  }

  function getProductByUPC($upc){
    $result = $this->connection->prepare("select * from product where Upc= :upc");
    $result->execute(array('upc' => $upc));
    $row = $result->fetch(PDO::FETCH_ASSOC);
    return new \Model\Product($row["Name"],$row["UPC"],$row["Inventory"],$row["Desc"]);
  }

  function getProductsByInventory($inventoryAmount){
    $result = $this->connection->prepare("select * from product where Inventory>= :inventory");
    $result->execute(array('inventory' => $inventoryAmount));
    $products = [];
    while($row->fetch(PDO::FETCH_ASSOC)){
      $products[] = new Product($row["Name"],$row["UPC"],$row["Inventory"],$row["Desc"]);
    }
    return $products;
  }
}

?>
