<?
/**
 *
 */
interface IDB
{
  function connectToDB();
  function getData($query);
  function setData($query);
}

class DBFactory {
  static function getDbInstance($db, $params) {
    switch ($db) {
      case 'mysql-product':
        return new MySqlProduct($params["host"],$params["port"],$params["user"],$params["password"]);
        break;
      default:
        return new MySqlProduct($params["host"],$params["port"],$params["user"],$params["password"]);
        break;
    }
  }
}


/**
* MySql implementation
*/
// class MySqlDB implements IDB{
//
//   private $host, $port, $user, $password, $connection;
//   function __construct($host, $port, $user, $password){
//     $this->host = $host;
//     $this->port = $port;
//     $this->user = $user;
//     $this->password = $password;
//   }
//
//   public function getData($query){
//     $result = $this->connection->query($query);
//     return $result;
//   }
//   public function setData($query){
//     echo "Setting data from $query";
//   }
//   /**
//   *@Return ConnectionPDO object
//   */
//   public function connectToDB(){
//     $dbConnection = new PDO("mysql:host=192.168.64.2;dbname=library", "temp", "Pa11word");
//     $this->connection = $dbConnection;
//   }
// }

?>
