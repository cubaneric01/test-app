<?php
namespace Model;

class Product {
  private $Name, $Upc, $Inventory, $Desc;

  function __construct($name, $upc, $inventory, $desc){
    $this->Name = $name;
    $this->Upc = $upc;
    $this->Inventory = $inventory;
    $this->Desc = $desc;
  }

  public function getName(){
    return $this->Name;
  }
  public function setName($name){
    $this->Name=$name;
  }
  public function getUpc(){
    return $this->Upc;
  }
  public function setUpc($upc){
    $this->Upc=$upc;
  }
  public function getInventory(){
    return $this->Inventory;
  }
  public function setInventory($inventory){
    $this->Inventory=$inventory;
  }
  public function getDesc(){
    return $this->Desc;
  }
  public function setDesc($desc){
    $this->Desc=$desc;
  }
}



?>
